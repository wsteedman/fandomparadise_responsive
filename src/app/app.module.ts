import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {CookieService} from 'ngx-cookie-service';
import {AnalyticsService} from './services/api/analytics.service';
import {AppComponent} from './app.component';
import {HttpClientModule} from '@angular/common/http';
import {LandingComponent} from './landing/landing.component';
import {NavbarComponent} from './shared/navbar/navbar.component';
import {routing} from './app.routing';
import {ProfileComponent} from './profile/profile.component';
import {RegisterComponent} from './profile/register/register.component';
import {ViewprofileComponent} from './profile/viewprofile/viewprofile.component';
import {ReactiveFormsModule, FormsModule} from '@angular/forms';
import {LogoutComponent} from './profile/logout/logout.component';
import {CarouselComponent} from './shared/carousel/carousel.component';
import {ApiService} from './services/api.service';
import {TriviaService} from './services/trivia/trivia.service';
import { ContentListComponent } from './shared/content-list/content-list.component';


@NgModule({
  declarations: [
    AppComponent,
    LandingComponent,
    NavbarComponent,
    ProfileComponent,
    RegisterComponent,
    ViewprofileComponent,
    LogoutComponent,
    CarouselComponent,
    ContentListComponent
  ],
  imports: [
    BrowserModule,
    routing,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [CookieService, AnalyticsService, ApiService, TriviaService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
