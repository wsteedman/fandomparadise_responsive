import {Component} from '@angular/core';
import { AnalyticsService } from './services/api/analytics.service';
import {NavigationEnd, Router} from '@angular/router';
import {CookieService} from 'ngx-cookie-service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  constructor(private api: AnalyticsService, private cookieService: CookieService, private router: Router ) {
    router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        api.logHit();
      }
    });
  }

  public logout()
  {
    this.cookieService.delete('sid');
    window.location.href = '/logout';
  }

}
