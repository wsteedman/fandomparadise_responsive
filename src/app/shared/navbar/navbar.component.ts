import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import {CookieService} from 'ngx-cookie-service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  public isLoggedIn: boolean = false;
  public username: string = '';

  constructor(private cookieService: CookieService) { }

  @Output() logoutEvent = new EventEmitter();

  ngOnInit() {
    const token = this.cookieService.get('sid');

    if (token) {
      const helper = new JwtHelperService();
      const decodedToken = helper.decodeToken(token);

      if (decodedToken && decodedToken.profile)
      {
        this.isLoggedIn = true;
        this.username = decodedToken.username;
      }
    }
  }

  public logout() {
    this.logoutEvent.emit();
  }

}
