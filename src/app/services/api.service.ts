import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpHeaders} from '@angular/common/http';
import {CookieService} from 'ngx-cookie-service';
import {HttpClient} from '@angular/common/http';

export enum ApiResponse {
  SUCCESS = -1,

  INVALID_REQUEST = 1000,
  INVALID_TOKEN = 1001,
  EXPIRED_TOKEN = 1002,

  PROFILE_ALREADY_EXISTS = 2001,
  EMAIL_ALREADY_EXISTS = 2002,
  INVALID_PASSWORD = 2003,
  INVALID_CREDENTIALS = 2004,

  DATABASE_ERROR = 5001,
  EMPTY_RESPONSE = 5002
}

@Injectable()
export class ApiService {
  constructor(private cookieService: CookieService) {}

  public getAuthHeader() {
    const session_id = this.cookieService.get('sid');
    let headers_object = new HttpHeaders();
    if (session_id) {
      headers_object = headers_object.set('Authorization', 'Bearer ' + session_id);
    }

    return headers_object;
  }

  private static services = {
    'responsive': 'rp/',
    'profile': 'profile/',
    'trivia': 'trivia/',
    'images': 'images/'
  };

  public static getFullUrl(service: string)
  {
    return environment.base_url + this.services[service];
  }
}
