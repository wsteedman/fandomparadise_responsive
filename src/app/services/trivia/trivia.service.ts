import { Injectable } from '@angular/core';
import {ApiService} from '../api.service';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class TriviaService {

  constructor(
    private apiService: ApiService,
    private http: HttpClient
  )
  {

  }

  public getTriviaBrands()
  {
    const headersObject = this.apiService.getAuthHeader();

    return this.http.get(
      ApiService.getFullUrl('trivia') + 'brands',
      {headers: headersObject}
    );
  }

}
