import {Injectable} from '@angular/core';
import {CookieService} from 'ngx-cookie-service';

import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {ApiResponse, ApiService} from '../api.service';

@Injectable()
export class AnalyticsService {

  private static pageName: string = 'Undefined';

  public static setPageName(pageName: string) {
    this.pageName = pageName;
  }

  private static metaData: {};

  public static setMetaData(metaData: object) {
    this.metaData = metaData;
  }

  constructor(
    private http: HttpClient,
    private cookieService: CookieService,
    private apiService: ApiService
  ) {
  }

  logHit(retry: boolean = false) {
    if (!retry && AnalyticsService.pageName === 'Undefined') {
      setTimeout(() => {
        this.logHit(true);
      }, 3000);
      return;
    }

    let headersObject = this.apiService.getAuthHeader();

    let request = this.http.post(
      ApiService.getFullUrl('responsive') + 'hit',
      {pn: AnalyticsService.pageName, md: AnalyticsService.metaData, ua: window.navigator.userAgent},
      {headers: headersObject}
    );

    request.subscribe((res: any) => {
      if (
        res.original && res.original.result &&
        res.original.result && (res.original.result.responseCode === ApiResponse.SUCCESS)
      ) {
        this.cookieService.set('sid', res.original.response, 0.5);
      }
    });
  };

  registerUser(username, email, password, confirm) {
    let headersObject = this.apiService.getAuthHeader();

    return this.http.post(
      ApiService.getFullUrl('profile') + 'register',
      {un: username, em: email, pw: password, cf: confirm},
      {headers: headersObject}
    ).pipe(
      map(res => {
        console.log(res);
        return res;
      })
    );
  }

  loginUser(email, password) {
    let headersObject = this.apiService.getAuthHeader();

    return this.http.post(
      ApiService.getFullUrl('profile') + 'login',
      {em: email, pw: password},
      {headers: headersObject}
    ).pipe(
      map(res => {
        console.log(res);
        return res;
      })
    );
  }

}
