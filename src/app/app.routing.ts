import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LandingComponent } from './landing/landing.component';
import { ProfileComponent } from './profile/profile.component';

const appRoutes: Routes =
  [
    {
      path: '',
      component: LandingComponent
    },
    {
      path: 'profile',
      component: ProfileComponent
    },
    {
      path: 'login',
      component: ProfileComponent
    },
    {
      path: 'logout',
      component: ProfileComponent,
      data: { loggedOut: true }
    }
  ];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
