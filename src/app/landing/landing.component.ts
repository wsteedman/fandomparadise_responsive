import {Component, OnInit} from '@angular/core';
import {AnalyticsService} from '../services/api/analytics.service';
import {Observable} from 'rxjs/internal/Observable';
import {TriviaService} from '../services/trivia/trivia.service';
import {ApiResponse, ApiService} from '../services/api.service';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css']
})
export class LandingComponent implements OnInit {

  public triviaBrands: any = [];
  public articles: any = [];

  constructor(private trivia: TriviaService) {
    AnalyticsService.setPageName('Landing');

    this.loadTriviaBrands();
    this.loadArticles();

  }

  private loadArticles()
  {
    this.articles = {
      featured: {
        title: 'Hogwarts Mystery: [Solved]',
        description: 'Come see why Hogwarts Mystery is a complete waste of time',
        image_path: 'http://fp_api.test/images/articles/hogwarts-mystery-solved/featured.jpg',
        link: 'articles/hogwarts-mystery-solved'
      },
      list: [
        {
          title: 'Can the Hulk beat Thanos?',
          description: 'Infinity War aside... Who is ACTUALLY stronger?',
          image_path: 'http://fp_api.test/images/articles/hulk-beat-thanos/thumbnail.jpg',
          link: 'articles/hulk-beat-thanos'
        },
        {
          title: 'Star Wars - Sequels vs Prequels',
          description: 'Nobody hates Star Wars like Star Wars fans...',
          image_path: 'http://fp_api.test/images/articles/star-wars-sequels-prequels/thumbnail.jpg',
          link: 'articles/star-wars-sequels-prequels'
        }
      ]
    };
  }

  private loadTriviaBrands()
  {
    this.trivia.getTriviaBrands().subscribe((res: any) => {
      if (res && res.result && res.result.responseCode && res.result.responseCode === ApiResponse.SUCCESS) {
        this.triviaBrands = res.response;
        this.triviaBrands = this.triviaBrands.map((brand: any) => {
          return {
            title: brand.title,
            image: ApiService.getFullUrl('images') + 'covers/' + brand.image_path,
            link: 'trivia/' + brand.slug,
            description: brand.description
          }
        });
        console.log(this.triviaBrands);
      }
    });
  }

  ngOnInit() {
  }

}
