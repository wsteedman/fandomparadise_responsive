import { Component, OnInit } from '@angular/core';
import { AnalyticsService } from '../services/api/analytics.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  public loggedOut = false;

  constructor( private activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
    let params = this.activatedRoute.snapshot.data;
    if (params && params['loggedOut']) {
      this.loggedOut = true;
    }
  }

}
