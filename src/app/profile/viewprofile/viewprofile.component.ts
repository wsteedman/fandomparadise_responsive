import { Component, OnInit } from '@angular/core';
import { AnalyticsService } from '../../services/api/analytics.service';

@Component({
  selector: 'app-viewprofile',
  templateUrl: './viewprofile.component.html',
  styleUrls: ['./viewprofile.component.css']
})
export class ViewprofileComponent implements OnInit {

  constructor() {
    AnalyticsService.setPageName('Profile - View Profile');
  }

  ngOnInit() {
  }

}
