import { Component, OnInit } from '@angular/core';
import { AnalyticsService } from '../../services/api/analytics.service';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

  constructor() {
    AnalyticsService.setPageName('Logout');
  }

  ngOnInit() {
  }

}
