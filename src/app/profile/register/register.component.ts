import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {AnalyticsService} from '../../services/api/analytics.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {CookieService} from 'ngx-cookie-service';
import {ApiResponse, ApiService} from '../../services/api.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  loginForm: FormGroup;
  loginSubmitted = false;
  registerSubmitted = false;

  register = {
    username: '',
    email: '',
    password: '',
    confirm: ''
  };

  login = {
    email: '',
    password: ''
  };

  public isLoading = false;

  constructor(private formBuilder: FormBuilder, private api: AnalyticsService,
              private router: Router, private cookieService: CookieService) {
    AnalyticsService.setPageName('Profile - Register/Login');
  }

  ngOnInit() {
    AnalyticsService.setPageName('Profile - Register/Login');
    this.loginForm = this.formBuilder.group({
      loginEmail: ['', [Validators.required, Validators.email]],
      loginPassword: ['', [Validators.required]]
    });
    this.registerForm = this.formBuilder.group({
      registerUsername: ['', Validators.required],
      registerEmail: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(8)]],
      repeatPassword: ['', Validators.required]
    }, {
      validator: RegistrationValidator.validate.bind(this)
    });
  }

  public registrationUserDuplicate: boolean = false;
  public registrationEmailDuplicate: boolean = false;
  public registrationInvalidPassword: boolean = false;

  public loginInvalidCredentials: boolean = false;

  get r() {
    return this.registerForm.controls;
  }

  get l() {
    return this.loginForm.controls;
  }

  loginEmailPassword() {
    this.loginSubmitted = true;
    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }
    this.isLoading = true;

    this.api.loginUser(
      this.login.email,
      this.login.password
    ).subscribe((res: any) => {
      this.isLoading = false;
      this.loginInvalidCredentials = true;

      if (res && res.result && res.result.responseCode && res.result.responseCode === ApiResponse.SUCCESS) {
        this.cookieService.set('sid', res.response.token, 30);
        window.location.href = '/';
      } else {
        alert('Oops! Looks like something went wrong...');
      }
    }, (err) => {
      this.isLoading = false;
      if (
        err.error.result && err.error.result.responseCode
      ) {
        switch (err.error.result.responseCode) {
          case ApiResponse.INVALID_CREDENTIALS:
            this.loginInvalidCredentials = true;
            break;
        }
      }
    });
  }

  registerUser() {
    this.registerSubmitted = true;
    this.registrationUserDuplicate = false;
    this.registrationEmailDuplicate = false;
    this.registrationInvalidPassword = false;
    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }
    this.isLoading = true;

    this.api.registerUser(
      this.register.username,
      this.register.email,
      this.register.password,
      this.register.confirm
    ).subscribe((res: any) => {
      this.isLoading = false;
      if (res && res.result && res.result.responseCode && res.result.responseCode === ApiResponse.SUCCESS) {
        this.cookieService.set('sid', res.response.token, 30);
        window.location.href = '/';
      } else {
        alert('Oops! Looks like something went wrong...');
      }
    }, (err) => {
      this.isLoading = false;
      if (
        err.error.result && err.error.result.responseCode
      ) {
        switch (err.error.result.responseCode) {
          case ApiResponse.PROFILE_ALREADY_EXISTS:
            this.registrationUserDuplicate = true;
            break;
          case ApiResponse.EMAIL_ALREADY_EXISTS:
            this.registrationEmailDuplicate = true;
            break;
          case ApiResponse.INVALID_PASSWORD:
            this.registrationInvalidPassword = true;
            break;
        }
      }
    });
  }

}

export class RegistrationValidator {
  static validate(registrationFormGroup: FormGroup) {
    let password = registrationFormGroup.controls.password.value;
    let repeatPassword = registrationFormGroup.controls.repeatPassword.value;

    if (repeatPassword.length <= 0) {
      return null;
    }

    if (repeatPassword !== password) {
      return {
        doesMatchPassword: true
      };
    }

    return null;

  }
}
